# Como executar esse projeto
<p>1. Clone o repositório: </p>
<p>2. execute o comando: composer update</p>
<p>3. execute o comando: php artisan migrate</p>
<p>4. execute o comando: npm run dev</p>
<p>5. execute o comando: php artisan serve.</p>
<p>6. Acesse o site pela URL: http://localhost:8000</p>

# Requisitos
<p>composer</p>
<p>laravel</p>
<p>node</p>

# Principais arquivos
<p>resources/js/components: componentes usados pelo app</p>
<p>resources/js/app.js: definição de rotas e uso do VueRouter</p>
<p>app/Http/Controllers/ItemController.php: definição de toda a regra para o crud</p>
<p>app/Http/Item.php: definição da classe usada para usar na migração e no CRUD</p>
<p>.env: configuração do banco de dados sqlite</p>